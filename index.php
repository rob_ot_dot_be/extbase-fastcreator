<?php
include_once('assets/classes/class.modelgenerator.php');

?>



<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">


    <title>Helpers for extbase</title>

    <!-- Bootstrap core CSS -->


    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <style type="text/css">

  body {
  padding-top: 30px;
}
.starter-template {
  padding: 40px 15px;
  text-align: center;
}
</style>


    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>



    <div class="container">

        <form class="form-horizontal" role="form" method="post" action="index.php">
            <div class="form-group" >
                <label for="pluginname" class="col-sm-2 control-label">tx_</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" value="valueexample" id="pluginname" name="pluginname" placeholder="pluginname">
                </div>
                <label for="modelname" class="col-sm-2 control-label">_domain_model_</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" value="modelexample" id="modelname" name="modelname" placeholder="modelname">
                </div>
            </div>

            <hr/>
            <div class="input_fields_wrap">
                <div class="row">
                    <div class="form-group" >
                        <label for="fname_1" class="col-sm-2 control-label">Field name <span>1</span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="fname_1" name="model[0][name]" placeholder="field name">
                            </div>
                        <div class="col-sm-2">
                            <select name="model[0][type]" id="ft1" class="form-control">
                                <option value="string">string</option>
                                <option value="integer">integer</option>
                                <option value="boolean">boolean</option>
                                <option value="model">model</option>
                            </select>

                        </div>
                  </div>
                </div>

            </div>
            <div class="row">
            <button id="add_field_button" class="btn btn-success pull-right">+ Add More Fields</button>
            </div>
            <div class="row">
                <button type="submit" class="btn btn-success">GENERATE</button>
            </div>
        </form>

        </div>

    <!-- /.container -->

    <div class="container">



            <?php
            if($_POST) {
                $newModel = new modelgenerator();
                echo '<h1>Properties</h1>';
                echo ' <pre>';
                foreach ($_POST['model'] as $model) {
                    $newModel->propgenerator($model['name'], $model['type']);
                }
                echo ' </pre>';

                echo '<h1>Setters / getters</h1>';
                echo ' <pre>';
                foreach ($_POST['model'] as $model) {
                    $newModel->settergenerate($model['name'], $model['type']);
                }
                echo ' </pre>';


                echo '<h1>TCA</h1>';
                echo ' <pre>';
                foreach ($_POST['model'] as $model) {
                    $newModel->tca($model['name'],$model['type'], $_POST['pluginname'], $_POST['modelname']);
                }
                echo ' </pre>';

                echo '<h1>SQL</h1>';
                echo ' <pre>';
                foreach ($_POST['model'] as $model) {
                    $newModel->sql($model['name']);
                }
                echo ' </pre>';

                echo '<h1>locallang_csh_tx_'.$_POST['pluginname'].'_domain_model_'.$_POST['modelname'].'.xlf</h1>';
                echo ' <pre>';
                foreach ($_POST['model'] as $model) {
                    $newModel->lang1($model['name'], $_POST['modelname']);
                }
                echo ' </pre>';

                echo '<h1>Locallang_db</h1>';
                echo ' <pre>';
                foreach ($_POST['model'] as $model) {
                    $newModel->lang2($model['name'], $_POST['pluginname'], $_POST['modelname']);
                }
                echo ' </pre>';
            }
            ?>

    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
   <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
   <script src="assets/scripts/script.js"></script>

  </body>
</html>
