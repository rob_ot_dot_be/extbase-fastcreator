<?php
class modelgenerator {
    function settergenerate($propName, $proptype)
    {
echo '
/**';
echo '
 * Sets the ' . $propName . '
 *
 * @param &#92;'.$proptype.' $' . $propName . '
 * @return void
 */
public function set' . ucfirst($propName) . '($' . $propName . ') {
    $this->' . $propName . ' = $' . $propName . ';
}

/**
 * Returns ' . $propName . '
 *
 * @return &#92;'.$proptype.' $' . $propName . '
 */
public function get' . ucfirst($propName) . '() {
    return $this->$' . $propName . ';
}
';
    }



    function propgenerator($propName, $proptype) {
        echo '
/**  ';
        echo '
 * '.$propName.'
 * @var &#92;'.$proptype.'
 */
protected $'.$propName.';
    ';
    }


        function tca($propName, $proptype, $pluginname, $modelname) {
echo "'$propName' => array(
'exclude' => 0,
'label' => 'LLL:EXT:uitleendienst/Resources/Private/Language/locallang_db.xlf:tx_".$pluginname."_domain_model_$modelname.$propName',
'config' => array(
'type' => 'input',
'size' => 30,
'eval' => 'trim'
),
),
<br/>";
}

    function decamelize($word) {
        return preg_replace(
            '/(^|[a-z])([A-Z])/e',
            'strtolower(strlen("\\1") ? "\\1_\\2" : "\\2")',
            $word
        );
    }



    function sql($propName) {
        echo strtolower( preg_replace( '/([A-Z])/', '_$1', lcfirst($propName ) ) )." varchar(255) DEFAULT '' NOT NULL, <br />";
}



    function lang1($propName,$modelname) {
       echo'
&#60;trans-unit id="'.$modelname.'.'.$propName.'"&#62;
    &#60;source&#62;'.strtolower( preg_replace( '/([A-Z])/', ' $1', lcfirst($propName ) ) ).'&#60;/source&#62;
&#60;/trans-unit&#62;
			';

    }


    function lang2($propName,$pluginname,$modelname) {
       echo'
&#60;trans-unit id="tx_'.$pluginname.'_domain_model_'.$modelname.'.'.$propName.'"&#62;
    &#60;source>'.strtolower( preg_replace( '/([A-Z])/', ' $1', lcfirst($propName ) ) ).'&#60;/source&#62;
&#60;/trans-unit&#62;

			';

    }

}





?>