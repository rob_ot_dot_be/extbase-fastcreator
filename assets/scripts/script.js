$(document).ready(function() {
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $("#add_field_button"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function(e){
        e.preventDefault();
            x++; //text box increment
            $(wrapper).append('<div class="row row'+x+'">'+
            '<div class="form-group" >'+
            '<label for="fname_'+x+'" class="col-sm-2 control-label">Field name <span>'+x+'</span></label>'+
            '<div class="col-sm-6">'+
           ' <input type="text" class="form-control" id="fname_'+x+'" name="model['+x+'][name]" placeholder="field name '+x+'">'+
            '</div>'+
            '<div class="col-sm-2">'+
            '<select name="model['+x+'][type]" id="ft'+x+'" class="form-control">'+
                '<option value="string">string</option>'+
                '<option value="integer">integer</option>'+
               ' <option value="boolean">boolean</option>'+
               ' <option value="model">model</option>'+
            '</select>'+
            '</div>'+
            '<div class="col-sm-2">'+
                '<button class="btn  btn-sm btn-danger remove_field" data-rem="row'+x+'">DEL</button>'+
            '</div>'+
            '</div>'+
            '</div>'
            ); //add input box

    });

    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault();
        $rem = $(this).data('rem');
        $('.'+$rem).remove();
    })

});